//
//  Unit.swift
//  Unit Converter
//
//  Created by Mohammad Faisal on 1/26/19.
//  Copyright © 2019 Mohammad Faisal. All rights reserved.
//

import Foundation

struct Unit: Decodable {
    
    let title: String?
    let symbol: String?
    let conversion: [Conversion]?
    
    init(title: String?, symbol: String?, conversion: [Conversion]?){
        self.title = title
        self.symbol = symbol
        self.conversion = conversion
    }
    
    enum CodingKeys: String, CodingKey {
        case title, symbol, conversion
    }
}
