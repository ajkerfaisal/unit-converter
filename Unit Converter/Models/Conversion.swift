//
//  Conversion.swift
//  Unit Converter
//
//  Created by Mohammad Faisal on 1/26/19.
//  Copyright © 2019 Mohammad Faisal. All rights reserved.
//

import Foundation

struct Conversion: Decodable {
    
    let symbol: String?
    let formula: String?
    
    init(symbol: String?, formula: String?){
        self.symbol = symbol
        self.formula = formula
    }
    
    enum CodingKeys: String, CodingKey {
        case symbol, formula
    }
}
