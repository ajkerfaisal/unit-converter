//
//  UnitHelper.swift
//  Unit Converter
//
//  Created by Mohammad Faisal on 1/26/19.
//  Copyright © 2019 Mohammad Faisal. All rights reserved.
//

import Foundation

class UnitHelper{
    
    static func getUnitDefinitions() -> [Unit]?{
        if let path = Bundle.main.path(forResource: "definition", ofType: "txt") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return try JSONDecoder().decode([Unit].self, from: data)
            } catch {
                print("unable to parse")
            }
        }
        return nil;
    }
    
    static func convert (formula: String, inputValue: Double) -> Double{
        if let result = NSExpression(format: formula).expressionValue(with: ["x" : inputValue], context: nil) as? Double {
            return result
        }else{
            print("something wrong with formula")
            return inputValue
        }
    }
}
