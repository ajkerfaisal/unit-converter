[
    {
        "title" : "Kelvin",
        "symbol" : "K",
        "conversion": [
            {
                "symbol" : "°C",
                "formula" : "x - 273.15"
            },
            {
                "symbol" : "°F",
                "formula" : "(x - 273.15) * 9/5 + 32"
            }
        ]
    },
    {
        "title" : "Celsius",
        "symbol" : "°C",
        "conversion": [
            {
                "symbol" : "K",
                "formula" : "x + 273.15"
            },
            {
                "symbol" : "°F",
                "formula" : "(x * 9/5) + 32"
            }
        ]
    },
    {
        "title" : "Fahrenheit",
        "symbol" : "°F",
        "conversion": [
            {
                "symbol" : "K",
                "formula" : "(x - 32) * 5/9 + 273.15"
            },
            {
                "symbol" : "°C",
                "formula" : "(x - 32) * 5/9"
            }
        ]
    },

    
    {
        "title" : "Litre",
        "symbol" : "L",
        "conversion": [
            {
                "symbol" : "mL",
                "formula" : "x * 1000"
            },
            {
                "symbol" : "US gal",
                "formula" : "x / 3.785"
            }
        ]
    },
    {
        "title" : "Millilitre",
        "symbol" : "mL",
        "conversion": [
            {
                "symbol" : "L",
                "formula" : "x / 1000"
            },
            {
                "symbol" : "US gal",
                "formula" : "x / 3785.412"
            }
        ]
    },
    {
        "title" : "US liquid gallon",
        "symbol" : "US gal",
        "conversion": [
            {
                "symbol" : "L",
                "formula" : "x  * 3.785"
            },
            {
                "symbol" : "mL",
                "formula" : "x * 3785.412"
            }
        ]
    }
]
