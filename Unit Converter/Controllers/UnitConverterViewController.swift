//
//  UnitConverterViewController.swift
//  Unit Converter
//
//  Created by Mohammad Faisal on 1/26/19.
//  Copyright © 2019 Mohammad Faisal. All rights reserved.
//

import UIKit
import iOSDropDown

class UnitConverterViewController: UIViewController {
    
    @IBOutlet weak var inputUnitDropDown: DropDown!
    @IBOutlet weak var inputValueTextField: UITextField!
    @IBOutlet weak var outputUnitDropDown: DropDown!
    @IBOutlet weak var outputValueLabel: UILabel!
    
    var units: [Unit] = []
    var selectedInputUnitIndex: Int?
    var selectedOutputUnitIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.units = UnitHelper.getUnitDefinitions()!
        self.setInputUnitDataSource()
    }
    
    func setInputUnitDataSource(){
        self.inputUnitDropDown.optionArray = self.units.map {$0.title!}
        self.inputUnitDropDown.didSelect{(selectedText , index ,id) in
            self.selectedInputUnitIndex = index
            self.setOutputUnitDataSource()           //setup output dropdown
        }
    }
    
    func setOutputUnitDataSource(){
        if self.selectedInputUnitIndex != nil {
            self.outputUnitDropDown.optionArray = (self.units[self.selectedInputUnitIndex!]).conversion!.map {
                self.getTitleForSymbol(symbol: $0.symbol!)
            }
            //reset output unit drop down
            self.outputUnitDropDown.text = nil
            self.outputUnitDropDown.selectedIndex = nil
            self.selectedOutputUnitIndex = nil
        }
        
        self.outputUnitDropDown.didSelect{(selectedText , index ,id) in
            self.selectedOutputUnitIndex = index
        }
    }
    
    func getTitleForSymbol(symbol: String) -> String{
        for unit in self.units{
            if unit.symbol == symbol{
                return unit.title!
            }
        }
        return symbol
    }
    
    @IBAction func convertButtonTapped(_ sender: Any) {
        //input validation
        if self.selectedInputUnitIndex == nil || self.selectedOutputUnitIndex == nil || inputValueTextField.text == ""{
            print("inputs are not all set. show alert to users")
            return;
        }
        
        let inputUnit = units[self.selectedInputUnitIndex!];
        let outputUnit = inputUnit.conversion![self.selectedOutputUnitIndex!]
        
        let result = UnitHelper.convert(formula: outputUnit.formula!, inputValue: Double(inputValueTextField.text!)!)
        self.outputValueLabel.text = "\(inputValueTextField.text ?? "") \(inputUnit.symbol ?? inputUnit.title!) = \(String(result.roundTo(places: 2))) \(outputUnit.symbol ?? "")"
    }
}

extension UnitConverterViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension Double {
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
