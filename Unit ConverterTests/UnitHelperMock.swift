//
//  UnitHelperMock.swift
//  Unit ConverterTests
//
//  Created by Mohammad Faisal on 1/26/19.
//  Copyright © 2019 Mohammad Faisal. All rights reserved.
//

import Foundation
@testable import Unit_Converter

class UnitHelperMock : UnitHelper{
    
    static func getFormula(inputUnitSymbol: String, outputUnitSymbol: String) -> String?{
        
        let units = getUnitDefinitions()!
        
        for unit in units{
            if unit.symbol == inputUnitSymbol {
                for conversion_unit in unit.conversion!{
                    if conversion_unit.symbol == outputUnitSymbol{
                        return conversion_unit.formula!
                    }
                }
            }
        }
        
        return nil;
    }
}
