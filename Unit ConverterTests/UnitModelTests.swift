//
//  UnitModelTests.swift
//  Unit ConverterTests
//
//  Created by Mohammad Faisal on 1/26/19.
//  Copyright © 2019 Mohammad Faisal. All rights reserved.
//

import XCTest
@testable import Unit_Converter

class UnitModelTests: XCTestCase {

    func testUnitDecodingCorrectFormat(){
        let data = """
        {
            "title" : "Kelvin",
            "symbol" : "K",
            "conversion": [
                {
                    "symbol" : "°C",
                    "formula" : "x - 273.15"
                },
                {
                    "symbol" : "°F",
                    "formula" : "(x - 273.15) * 9/5 + 32"
                }
            ]
        }
        """.data(using: .utf8)!
        
        do {
            let unit = try JSONDecoder().decode(Unit.self, from: data)
            XCTAssertNotNil(unit, "unit should not be nil")
            XCTAssertEqual(unit.title, "Kelvin", "Should be Kelvin")
        } catch {
            XCTFail("could not parse")
        }
    }
    
    func testUnitDecodingInvalidFormat(){
        let data = """
        {
            "title" : 1,
            "symbol" : 27,
            "conversion": "Kelvin to Celsius"
        }
        """.data(using: .utf8)!

        XCTAssertThrowsError(try JSONDecoder().decode(Unit.self, from: data), "parsing exception wasn't thrown")
    }

}
