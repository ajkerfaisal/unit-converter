//
//  UnitHelperTests.swift
//  Unit ConverterTests
//
//  Created by Mohammad Faisal on 1/26/19.
//  Copyright © 2019 Mohammad Faisal. All rights reserved.
//

import XCTest
@testable import Unit_Converter

class UnitHelperTests: XCTestCase {

    let units = UnitHelper.getUnitDefinitions()!
    
    func testGetUnitDefinitions(){
        let units = UnitHelper.getUnitDefinitions()
        XCTAssertNotNil(units, "definitions should not be nil")
    }
    
    //temperature: Kelvin 2 Celsius
    func testConvertKelvinToCelsius(){
        let kelvin = units[0]
        let celsius = kelvin.conversion![0]
        
        let result = UnitHelper.convert(formula: celsius.formula!, inputValue: 300.0)
        
        XCTAssertEqual(result, 26.85, accuracy: 0.01, "300 K should be 26.85 C")
    }
    
    //UnitHelperMock: Fahrenheit 2 Celsius
    func testConvertFahrenheitToCelsius(){
        let formula = UnitHelperMock.getFormula(inputUnitSymbol: "°F", outputUnitSymbol: "°C")
        
        XCTAssertNotNil(formula, "formula should not be nil")
        
        let result = UnitHelper.convert(formula: formula!, inputValue: 102.0)
        XCTAssertEqual(result, 38.88, accuracy: 0.01, "102 F should be 38.88 Celsius")
    }
    
    //volume: Litre 2 US Gallon
    func testConvertLitreToUSGallon(){
        let litre = units[3]
        let us_gal = litre.conversion![1]
        
        let result = UnitHelper.convert(formula: us_gal.formula!, inputValue: 27.9)
        
        XCTAssertEqual(result, 7.37, accuracy: 0.01, "27.9 L should be 07.37 US gal")
    }
    
    func testGetUnitDefinitionsPerformance() {

        self.measure {
            let _ = UnitHelper.getUnitDefinitions()
        }
    }

}
